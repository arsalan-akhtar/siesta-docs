Sphinx==3.3.1
sphinx-autobuild==2020.9.1
sphinx-inline-tabs
sphinxcontrib-mermaid==0.5.0
sphinx-notfound-page==0.6

sphinx-rtd-theme==0.5.0

# For spelling
sphinxcontrib-spelling==7.0.1
pyenchant==3.1.1
