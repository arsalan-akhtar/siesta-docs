.. _reference:

Technical reference
===================

These are sample reference manuals, translated with pandoc from their LaTeX
source files. Many formatting issues remain.

Siesta Manual
-------------------------

.. toctree::
    :maxdepth: 1

    siesta


Utilities
-----------------------

.. toctree::
    :maxdepth: 1

    denchar.rst
    macroave.rst
    plstm.rst
    wfs2ldos.rst
	       
Other
-----------------------

.. toctree::
    :maxdepth: 1

    fdf-file.rst
    output-files.rst
    
