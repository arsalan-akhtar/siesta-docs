:sequential_nav: next

..  _tutorial-advanced-analysis:

Advanced analysis of the electronic structure
=============================================

.. 
   This will contain the COOP/COHP bonding analysis, the fat-bands, and
   the spin texture processing. All three share a basis of theoretical
   concepts (use of wavefunction coefficients as starting point), and actually
   reside in the same place (Util/COOP) in the distribution.

   In addition, the Mulliken charges will appear, leading also to
   mentions of the Hirshfeld, Voronoi, and Bader schemes.


In the basic tutorials we have seen ways to "see" the electronic
structure via DOS plots, band structures, or wavefunction
pictures. Here we take a deeper view, using as basic ingredients the
coefficients of the wavefunctions and the matrix elements of the
hamiltonian and overlap matrix.

.. Chain of equations for decomposition of N and E_BS, etc... will
   follow.

   N = \int_0^{\varepsilon_F} { d\varepsilon
        \sum_i {
	   \sum_{\mu} {
              \sum_{\nu} {
	          c_{i\mu}c_{i\nu} S_{\mu\nu}
		          \delta(\varepsilon - \varepsilon_i)
		         }
	              }
	       }              }

   Similarly, for the band-structure energy:

   E_{BS} = \int_0^{\varepsilon_F} { d\varepsilon
        \sum_i {
	   \sum_{\mu} {
              \sum_{\nu} {
	          c_{i\mu}c_{i\nu} H_{\mu\nu}
		          \delta(\varepsilon - \varepsilon_i)
		         }
	              }
	       }              }


.. toctree::
    :maxdepth: 1

    fatbands/index
    coop-cohp/index
    spin-texture/index

    
	
