:sequential_nav: next

..  _tutorial-coop-cohp:

Bonding analysis through crystal overlap populations (COOP/COHP)
================================================================


Background
----------

One can obtain a lot of insight into the chemical bonding from a
straightforwad analysis of the decomposition of the band-structure
energy into the contributions of individual pairs of atoms (or
particular orbitals associated with them).


See the `LOBSTER site <http://www.cohp.de/>`_ for more
information. LOBSTER is a program created by the group of Richard
Dronskowski, one of the pioneers of this kind of bonding analysis in
solid-state chemistry.



COOP and COHP curves for a chain of N atoms 
--------------------------------------------

.. hint::
   Enter directory *N_chain*
   
Run Siesta on the n_chain.fdf file.

Note the 'COOP.Write T' line in the file. This forces the creation of
the files n_chain.fullBZ.WFSX and n_chain.HSX, which contain wave-function
and Hamiltonian and Overlap information, respectively.

These files can be processed by the mprop program (full documentation
available typing ``mprop -h``) to obtain the DOS, PDOS, COOP, and COHP
curves for an arbitrary combination of orbitals.

Before using mprop, issue the following command to link the WFSX file to
the name expected by the program::

   ln -sf n_chain.fullBZ.WFSX n_chain.WFSX

Then type::

   mprop pdos

to generate PDOS information (see file pdos.mpr). The
curves can be plotted with the pdos.gplot script::

  gnuplot --persist  pdos.gplot

Type::

   mprop n_coo

to generate COOP and COHP curves. These display the
energy-resolved bonding and antibonding character in the system.

Spin instability in "non-magnetic Iron"
---------------------------------------

This example is inspired by the discussion in LOBSTER's website.
One can compute the electronic structure of a hypothetical phase of
iron *without spin polarization*, i.e., non-magnetic. While it is
obvious that the total energy of this system is higher than that of
the true, spin-polarized ground state, one can look at the COOP/COHP
curves to get a deeper view.

(**Full description forthcoming**)




References
----------

* Hoffmann, Roald. 1987. *How Chemistry and Physics Meet in the Solid
  State.*, `Angewandte Chemie International Edition in English 26 (9):
  846–78. <https://doi.org/10.1002/anie.198708461>`_

* R. Dronskowski, P. E. Blöchl, J. Phys. Chem. 1993, 97, 8617–8624 [COHP
  definition];

* Sanchez-Portal, Daniel, Emilio Artacho, and Jose M
  Soler. 1995. `“Projection of Plane-Wave Calculations into Atomic
  Orbitals.” Solid State Communications 95 (10):
  685–90. <https://doi.org/10.1016/0038-1098(95)00341-X>`_ 

More LOBSTER references:

* V. L. Deringer, A. L. Tchougréeff, R. Dronskowski, J. Phys. Chem. A
  2011, 115, 5461–5466 [projected COHP definition];
  
* S. Maintz, V. L. Deringer, A. L. Tchougréeff, R. Dronskowski, J. Comput. Chem. 2013,
  34, 2557-2567 [The mathematical apparatus and the framework on which
  LOBSTER is built].
  
* S. Maintz, V. L. Deringer, A. L. Tchougréeff, R. Dronskowski, J. Comput. Chem. 2016,
  37, 1030-1035 [LOBSTER 2.0.0 and its nuts and bolts].

