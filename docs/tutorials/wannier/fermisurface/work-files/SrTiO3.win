num_bands       = 12     ! Number of bands in the first-principles calculation
                         !   used to form the overlap matrices 
                         !   seedname.mmn file
                         !   Default: num_bands = num_wann
num_wann        = 12     ! Number of Wannier functions to be found
                         !   There must be as many projectors in the 
                         !   projections bloch as num_wann functions here.
                         !   If num_wann < num_bands, then the disentaglement
                         !   procedure is activated 
                         ! In this example, we want to Wannierize the 
                         !   top of the valence bands (O 2p in character)
                         !   and the bottom of the conduction bands (Ti t2g),
                         !   so it amounts to 
                         !   - O p bands (3 bands * 3 O atoms)
                         !   - Ti t2g bands (3 bands: dxy, dyz, dxz)
                         !   Twelve bands in total
!
! For every k-point, the bands in Siesta are ordered from lowest to highest 
! energy
! The character of the bands are known after computation of the PDOS.
! In order, for the case of SrTiO3:
! -----------------------------------------------------------------------
! Bands to be excluded
! -----------------------------------------------------------------------
! Band  1 (lowest band):  Ti-3s character 
! Bands 2, 3, and 4:      Ti-3p character (px, py, and pz) 
! Band  5:                Sr-4s character
! Bands 6, 7, and 8:      O-2s  character (1 band * 3 O atoms in the unit cell)
! Bands 9, 10, and 11:    Sr-4p character (px, py, and pz)
! -----------------------------------------------------------------------
! Bands to be wannierized
! -----------------------------------------------------------------------
! Bands 12-20:            O-2p  character (3 bands * 3 O atoms in the unit cell)
! Bands 21, 22, and 23:   Ti-t2g characer (dxy, dyz, dxz)

! In exclude_bands, we list the bands to be excluded from the calculation
! Here we exclude all bands except the O2p and Ti t2g bands.
! That means that we exclude:
! - Ti 3s band   (1 band)
! - Ti 3p bands  (3 bands)
! - Sr 4s bands  (1 band)
! - Sr 4p bands  (3 bands)
! - O 2s bands   (1 band * 3 atoms = 3 bands) 
! This amounts to the first eleven bands for every k-point.
exclude_bands : 1,2,3,4,5,6,7,8,9,10,11

guiding_centres = T      ! The projection centres are used as the guiding
                         !   centres in the Wannierisation routines  

begin projections        ! The projections block defines a set of 
O:p                      !   localised functions used to generate an 
Ti:l=2,mr=2,3,5          !   initial guess for the unitary transformations.
end projections

begin unit_cell_cart
Ang
3.874 0.000 0.000
0.000 3.874 0.000
0.000 0.000 3.874
end unit_cell_cart

begin atoms_frac
Sr 0.0   0.0   0.0
Ti 0.5   0.5   0.5
O  0.5   0.5   0.0
O  0.5   0.0   0.5
O  0.0   0.5   0.5
end atoms_frac

begin kpoint_path                          ! Defines the path in k-space along 
G 0.000  0.000 0.000 X 0.500  0.000 0.000  ! which to calculate the 
X 0.500  0.000 0.000 M 0.500  0.500 0.000  ! bandstructure. Each line gives the
M 0.500  0.500 0.000 G 0.000  0.000 0.000  ! start and end point (with labels) 
G 0.000  0.000 0.000 R 0.500  0.500 0.500  ! for a section of the path. 
R 0.500  0.500 0.500 X 0.500  0.000 0.000  ! Values are in fractional 
end kpoint_path                            ! coordinates with respect to the 
                                           ! primitive reciprocal lattice 
                                           ! vectors.
bands_plot =T

!To plot the WF
wannier_plot = T            ! If true, then the code will write out the 
                            !   Wannier functions in a super-cell whose size is
                            !   defined by the variable wannier_plot_supercell, 
                            !   and in a format specified by wannier_plot_format
wannier_plot_supercell = 3
wannier_plot_list = 1,2,10  ! A list of WF to plot. 
                            !   The WF numbered as per the seedname.wout 
                            !   file after the minimisation of the spread.
fermi_energy   = -4.747334  ! The value of the Fermi energy (-4.747334 eV) 
                            ! was obtained from the initial first-principles
                            ! simulations with Siesta
fermi_surface_plot = true   ! This makes Wannier90 to generate the 
                            ! Fermi surface for SrTiO3


mp_grid : 4 4 4  

begin kpoints
  0.00000000  0.00000000  0.00000000 
  0.00000000  0.00000000  0.25000000 
  0.00000000  0.00000000  0.50000000 
  0.00000000  0.00000000  0.75000000 
  0.00000000  0.25000000  0.00000000 
  0.00000000  0.25000000  0.25000000 
  0.00000000  0.25000000  0.50000000 
  0.00000000  0.25000000  0.75000000 
  0.00000000  0.50000000  0.00000000 
  0.00000000  0.50000000  0.25000000 
  0.00000000  0.50000000  0.50000000 
  0.00000000  0.50000000  0.75000000 
  0.00000000  0.75000000  0.00000000 
  0.00000000  0.75000000  0.25000000 
  0.00000000  0.75000000  0.50000000 
  0.00000000  0.75000000  0.75000000 
  0.25000000  0.00000000  0.00000000 
  0.25000000  0.00000000  0.25000000 
  0.25000000  0.00000000  0.50000000 
  0.25000000  0.00000000  0.75000000 
  0.25000000  0.25000000  0.00000000 
  0.25000000  0.25000000  0.25000000 
  0.25000000  0.25000000  0.50000000 
  0.25000000  0.25000000  0.75000000 
  0.25000000  0.50000000  0.00000000 
  0.25000000  0.50000000  0.25000000 
  0.25000000  0.50000000  0.50000000 
  0.25000000  0.50000000  0.75000000 
  0.25000000  0.75000000  0.00000000 
  0.25000000  0.75000000  0.25000000 
  0.25000000  0.75000000  0.50000000 
  0.25000000  0.75000000  0.75000000 
  0.50000000  0.00000000  0.00000000 
  0.50000000  0.00000000  0.25000000 
  0.50000000  0.00000000  0.50000000 
  0.50000000  0.00000000  0.75000000 
  0.50000000  0.25000000  0.00000000 
  0.50000000  0.25000000  0.25000000 
  0.50000000  0.25000000  0.50000000 
  0.50000000  0.25000000  0.75000000 
  0.50000000  0.50000000  0.00000000 
  0.50000000  0.50000000  0.25000000 
  0.50000000  0.50000000  0.50000000 
  0.50000000  0.50000000  0.75000000 
  0.50000000  0.75000000  0.00000000 
  0.50000000  0.75000000  0.25000000 
  0.50000000  0.75000000  0.50000000 
  0.50000000  0.75000000  0.75000000 
  0.75000000  0.00000000  0.00000000 
  0.75000000  0.00000000  0.25000000 
  0.75000000  0.00000000  0.50000000 
  0.75000000  0.00000000  0.75000000 
  0.75000000  0.25000000  0.00000000 
  0.75000000  0.25000000  0.25000000 
  0.75000000  0.25000000  0.50000000 
  0.75000000  0.25000000  0.75000000 
  0.75000000  0.50000000  0.00000000 
  0.75000000  0.50000000  0.25000000 
  0.75000000  0.50000000  0.50000000 
  0.75000000  0.50000000  0.75000000 
  0.75000000  0.75000000  0.00000000 
  0.75000000  0.75000000  0.25000000 
  0.75000000  0.75000000  0.50000000 
  0.75000000  0.75000000  0.75000000 
end kpoints
