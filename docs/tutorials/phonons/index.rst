:sequential_nav: next

..  _tutorial-advanced-phonons:

Advanced topics in phonons
==========================


.. note::
   For background, see the
   excellent `slide presentation <https://drive.google.com/file/d/1SRhSOWwFVPmS3vr4REJSvqgXf2Tic8fl/view?usp=sharing>`_
   by Andrei Postnikov. 

This intermediate/advanced tutorial will focus on topics such as:

   * Post-processing with Andrei Postnikov's lattice-dynamics tools.
   * Phonon (projected)DOS
   * Born-effective charges and infrared activity.

..  Exercises: Use the newer examples by Andrei on a BN
    monolayer and a Au chain.

..  For LO-TO and infrared, we need examples. Actually, for LO-TO
    splitting we need to fish out Thomas Archer's vibra code.

   
   
